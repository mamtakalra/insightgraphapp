<?php
session_start();
$pageId="517225458339386";



$date1=strtotime('2013-11-12 14:35:08');
$date2=strtotime('2014-01-04 14:35:08');

$param="since=" .$date1 . "&until=" .$date2;

 

$url="https://graph.facebook.com/". $pageId ."/insights/page_impressions_organic?" . $param . "&access_token=".$_SESSION["token"];
 


$test=file_get_contents($url,true);


$data = json_decode($test, true);
echo "<pre>";
//print_r($data[data]);
echo "</pre>";

$url1="https://graph.facebook.com/". $pageId ."/insights/page_impressions_paid?" . $param . "&access_token=".$_SESSION["token"];
$test1=file_get_contents($url1,true);


$data2 = json_decode($test1, true);

 $totalvalues=count($data[data][0][values]);

 $source='';
 $likesSummary='[';
 for($i=0;$i<count($data[data][0][values]);$i++)
 {
		//echo $data[data][0][values][$i][value] . "<br /><hr />";

		$likesSummary=$likesSummary . '{' . '"year":' . $i . ', "organic":' .  $data[data][0][values][$i][value] . ', "paid":' .  $data2[data][0][values][$i][value] . '},'; 




	


 }


 $likesSummary=$likesSummary ."]";

//echo $likesSummary;






?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Post Reach Facebook</title>
        <link rel="stylesheet" href="style.css" type="text/css">
        <script src="amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/serial.js" type="text/javascript"></script>         
        <script type="text/javascript">
            
            // this chart is exactly the same as in areaStacked.html, only made using JSON except JavaScript API

            var chartData = <?php echo $likesSummary; ?>;
           AmCharts.makeChart("chartdiv", {
               type: "serial",
               pathToImages: "amcharts/images/",
               dataProvider: chartData,
               marginTop: 10,
               categoryField: "year",
               categoryAxis: {
                   gridAlpha: 0.07,
                   axisColor: "#DADADA",
                   startOnAxis: true
                  
               },
               valueAxes: [{
                   stackType: "regular",
                   gridAlpha: 0.07,
                   title: "Likes"
               }],

               graphs: [{
                   type: "line",
                   title: "Organic",
                   hidden: false,
                   valueField: "organic",
                   lineAlpha: 0,
                   fillAlphas: 0.6,
                   balloonText: "<img src='images/car.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>"
               }, {
                   type: "line",
                   title: "Paid",
                   valueField: "paid",
                   lineAlpha: 0,
                   fillAlphas: 0.6,
                   balloonText: "<img src='images/motorcycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>"
               }

			   ],
               legend: {
                   position: "top",
                   valueText: "[[value]]",
                   valueWidth: 100,
                   valueAlign: "left",
                   equalWidths: false,
                   periodValueText: "total: [[value.sum]]"
               },
               chartCursor: {
                   cursorAlpha: 0
               },
               chartScrollbar: {
                   color: "FFFFFF"
               }

           });

        </script>
    </head>
    
    <body>

	<h1 style="text-align:center;">REACH</h1>

	
		
		<div style="background:url(images/reach.jpg);width:1000px;height:606px;margin-left:20%;">
        <div id="chartdiv" style="width:82%; height:400px;text-align:center;padding-top:9%;padding-right:3%;"></div>

		</div>
    </body>

</html>